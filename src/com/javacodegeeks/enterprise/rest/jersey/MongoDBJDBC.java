package com.javacodegeeks.enterprise.rest.jersey;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;


@Path("/files")
public class MongoDBJDBC {
	 String password="WelcomeToSona";
	 private static String DATABASE = "Finance";
	 private static String COLLECTION = "Register";
	 
	
  @POST
  @Path("/insert")  
  	    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  		@Produces("text/html")
  	    public Response addUser(  
  	        @FormParam("name") String name,  
  	        @FormParam("email") String email ) throws Exception{
    			
    	      try{   
    	    	 
    			
    	         // To connect to mongodb server
    	         MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
    			 // Now connect to your databases
    	         DB db = mongoClient.getDB(DATABASE);
    	         System.out.println("Connect to Finance database successfully");
    	       
    	         DBCollection register = db.getCollection(COLLECTION);
    	         System.out.println("Collection register selected successfully");
    				
    	         BasicDBObject obj = new BasicDBObject();
    	        		 obj.append("name", name);
    	            obj.append("email", email);
    	            obj.append("password", password);
    	            WriteResult result = register.insert(obj);
    	            Sendmail(email);
    	         return Response.ok("Document inserted successfully").build();
    	     }catch(Exception e){
    	    	 return Response.ok("Document not inserted successfully").build();
    	     }
  		}
  
      public void Sendmail(String to)
      {
    	  String  d_email = "webdemo11@gmail.com",
                  d_password = "unisoftindia",
                  d_host = "smtp.gmail.com",
                  d_port  = "465",
                  m_to = to,
                  m_subject = "Welcome to SonaSoft",
                  m_text = "You have successfully registered to SonaSoft"
                  		+ " Your default passsword is:" + password + " Reset your password here: ";
    	
          Properties props = new Properties();
          props.put("mail.smtp.user", d_email);
          props.put("mail.smtp.host", d_host);
          props.put("mail.smtp.port", d_port);
          props.put("mail.smtp.starttls.enable","true");
          props.put("mail.smtp.auth", "true");
          props.put("mail.smtp.debug", "true");
          props.put("mail.smtp.socketFactory.port", d_port);
          props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
          props.put("mail.smtp.socketFactory.fallback", "false");

          //SecurityManager security = System.getSecurityManager();
          try
          {
        	  Session session = Session.getInstance(props,
        		      new javax.mail.Authenticator() {
        		         protected PasswordAuthentication getPasswordAuthentication() {
        		            return new PasswordAuthentication(d_email, d_password);
        		         }
        		      });
              session.setDebug(true);

              Message messageSSL = new MimeMessage(session);
              String content=m_text + "<a href=\"http://localhost:8080/Registration/ResetPassword.jsp"+"\">Click here to reset your password</a>";
              messageSSL.setContent(content, "text/html");   
        	  messageSSL.setSubject(m_subject);
        	  messageSSL.setFrom(new InternetAddress(d_email));
        	  messageSSL.addRecipient(Message.RecipientType.TO, new InternetAddress(m_to));
              Transport.send(messageSSL);
          }
          catch (Exception m)
          {
              m.printStackTrace();
          } 
      
      }
  	
  	@POST
  	@Path("/updatePassword")
  			@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  			@Produces("text/html")
  			public Response updatePassword(
  					@FormParam("email") String email,  
  					@FormParam("password") String newPassword ) throws Exception{
  		try{   
  			
  	         // To connect to mongodb server
  	         MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
  				
  	         // Now connect to your databases
  	         DB db = mongoClient.getDB( DATABASE );
  	         System.out.println("Connect to database successfully");
  	         
  	         System.out.println("Collection register selected successfully");
  	        DBCollection register = db.getCollection(COLLECTION);
  	        DBObject query = new BasicDBObject("email", email);
 	        DBObject update = new BasicDBObject();
 	        update.put("$set", new BasicDBObject("password", newPassword));
 	        WriteResult result = register.update(query, update);
 	       mongoClient.close();
 	        return Response.ok("Update Successful").build();
 	        
  	      }catch(Exception e){
  	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
  	       return Response.ok("Update UnSuccessful").build();
  	      }
  	   
  	
  		
  	}
}
  	

    	

   


